package jp.pocketbackup.sample.domain.sample;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Samples implements Serializable {

	private List< Sample > list = new ArrayList<>();

	private static final long serialVersionUID = 4041866022660503955L;
}
