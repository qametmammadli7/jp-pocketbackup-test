package jp.pocketbackup.sample.domain.sample;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Sample implements Serializable {

	private Id				id				= new Id();
	private EmailAddress	emailAddress	= new EmailAddress();
	private CreatedAt		createdAt		= new CreatedAt();

	private static final long serialVersionUID = 7172488927851269433L;
}
