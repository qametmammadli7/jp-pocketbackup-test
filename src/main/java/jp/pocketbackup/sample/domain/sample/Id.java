package jp.pocketbackup.sample.domain.sample;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Id implements Serializable {

	@Pattern( regexp = "\\d+", message = "IDを半角数字で入力してください。" )
	private String	input	= "";
	private Long	value	= 0L;

	public void setInput( String input ) {
		try {
			this.input = input;
			this.value = Long.parseLong( input );
		} catch ( Exception exception ) {
			this.value = 0L;
		}
	}

	private static final long serialVersionUID = 7139157613485491128L;
}
