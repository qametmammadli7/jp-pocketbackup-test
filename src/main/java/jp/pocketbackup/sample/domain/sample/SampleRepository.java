package jp.pocketbackup.sample.domain.sample;

public interface SampleRepository {

	Samples findAll();

	Sample findById( Id id );

}
