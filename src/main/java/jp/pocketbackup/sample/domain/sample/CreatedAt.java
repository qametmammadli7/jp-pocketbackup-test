package jp.pocketbackup.sample.domain.sample;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreatedAt implements Serializable {

	private LocalDateTime value = LocalDateTime.now();

	public String getFormattedValue() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern( "yyyy/MM/dd HH:mm:ss" );
		return this.value.format( formatter );
	}

	private static final long serialVersionUID = 452883840289156557L;
}
