package jp.pocketbackup.sample.domain.sample;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmailAddress implements Serializable {

	@NotBlank( message = "メールアドレスを入力してください。" )
	private String value = "";

	private static final long serialVersionUID = -3746030550970958080L;
}
