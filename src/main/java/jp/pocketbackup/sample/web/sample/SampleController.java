package jp.pocketbackup.sample.web.sample;


import jp.pocketbackup.sample.domain.sample.EmailAddress;
import jp.pocketbackup.sample.domain.sample.Id;
import jp.pocketbackup.sample.domain.sample.Sample;
import jp.pocketbackup.sample.domain.sample.SampleRepository;
import jp.pocketbackup.sample.domain.sample.Samples;
import jp.pocketbackup.sample.service.sample.SendEmailService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class SampleController {

	private static final Logger logger = LogManager.getLogger();

	@Autowired
	private SendEmailService service;

	@Autowired
	private SampleRepository repository;

	@GetMapping( value = "" )
	public String root( Model model ) throws Exception {
		logger.debug( "called. #root" );
		model.addAttribute( "id", new Id() );
		model.addAttribute( "emailAddress", new EmailAddress() );
		return "page/sample";
	}

	@PostMapping( value = "email/send" )
	public String emailSend( Model model, @Valid EmailAddress emailAddress, BindingResult bindingResult, RedirectAttributes redirectAttributes ) {
		logger.debug( "called. #emailSend" );

		if ( bindingResult.hasErrors() ) {
			model.addAttribute( "id", new Id() );
			return "page/sample";
		}

		service.send( emailAddress );
		redirectAttributes.addFlashAttribute( "message", "メールを送信しました。" );
		return "redirect:/";
	}

	@GetMapping( value = "db/select/all" )
	public String dbSelectAll( Model model ) {
		logger.debug( "called. #dbSelectAll" );
		Samples samples = repository.findAll();
		model.addAttribute( "samples", samples );
		return "page/result";
	}

	@PostMapping( value = "db/select/id" )
	public String dbSelectId( Model model, @Valid Id id, BindingResult bindingResult ) {
		logger.debug( "called. #dbSelectId" );

		if ( bindingResult.hasErrors() ) {
			model.addAttribute( "emailAddress", new EmailAddress() );
			return "page/sample";
		}

		Sample sample = repository.findById( id );
		model.addAttribute( "sample", sample );
		return "page/result";
	}
}
