package jp.pocketbackup.sample.service.sample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.HashMap;

@Service
public class ConvertThymeleafTemplateService {

	@Autowired
	private TemplateEngine textTemplateEngine;

	public String convert( String template, HashMap<String, Object> map ) {
		Context context = new Context();
		for ( String key : map.keySet() ) {
			context.setVariable( ( String ) key, map.get( key ) );
		}
		String result = textTemplateEngine.process( template, context );
		return result;
	}
}
