package jp.pocketbackup.sample.service.sample;

import jp.pocketbackup.sample.domain.sample.EmailAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class SendEmailService {

	@Value( "${spring.mail.from}" )
	private String from;

	@Value( "${spring.mail.cc}" )
	private String cc;

	@Value( "${spring.mail.bcc}" )
	private String bcc;

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private ConvertThymeleafTemplateService convertThymeleafTemplateService;

	public void send( EmailAddress emailAddress ) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom( from );
		message.setTo( emailAddress.getValue() );
		if ( cc.isEmpty() == false ) {
			message.setBcc( cc.split( "," ) );
		}
		if ( bcc.isEmpty() == false ) {
			message.setBcc( bcc.split( "," ) );
		}
		message.setSubject( "【ポケットバックアップ】サンプルメール送信完了のお知らせ" );
		message.setText( convertTemplate( emailAddress ) );

		javaMailSender.send( message );
	}

	private String convertTemplate( EmailAddress emailAddress ) {
		HashMap< String, Object > map = new HashMap< String, Object >();
		map.put( "emailAddress", emailAddress );
		String template = "email/sample";
		return convertThymeleafTemplateService.convert( template, map );
	}
}
