package jp.pocketbackup.sample.configuration;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@MapperScan( "jp.pocketbackup.sample.datasource" )
@PropertySource("classpath:config/application-development.properties")
public class DataSourceConfiguration {

	@Value( "${spring.datasource.driver-class-name}" )
	private String driverClassName;

	@Value( "${spring.datasource.url}" )
	private String url;

	@Value( "${spring.datasource.username}" )
	private String username;

	@Value( "${spring.datasource.password}" )
	private String password;

	@Value( "${spring.datasource.characterEncoding}" )
	private String characterEncoding;

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl( url );
		dataSource.setUsername( username );
		dataSource.setPassword( password );
		dataSource.setDriverClassName( driverClassName );
		Properties properties = new Properties();
		properties.setProperty( "characterEncoding", characterEncoding );
		dataSource.setConnectionProperties( properties );
		return dataSource;
	}

	@Bean
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
		sessionFactory.setDataSource( dataSource() );
		return sessionFactory.getObject();
	}
}
