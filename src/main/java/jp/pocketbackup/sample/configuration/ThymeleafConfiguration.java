package jp.pocketbackup.sample.configuration;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.nio.charset.StandardCharsets;

@Configuration
public class ThymeleafConfiguration implements ApplicationContextAware, EnvironmentAware {

	private ApplicationContext	applicationContext;
	@SuppressWarnings( "unused" )
	private Environment			environment;

	@Bean
	public TemplateEngine htmlTemplateEngine() {
		TemplateEngine templateEngine = new TemplateEngine();
		templateEngine.addTemplateResolver( htmlTemplateResolver() );
		return templateEngine;
	}

	@Bean
	public TemplateEngine textTemplateEngine() {
		TemplateEngine templateEngine = new TemplateEngine();
		templateEngine.addTemplateResolver( textTemplateResolver() );
		return templateEngine;
	}

	private ITemplateResolver htmlTemplateResolver() {
		SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
		resolver.setApplicationContext( this.applicationContext );
		resolver.setPrefix( "classpath:/templates/" );
		resolver.setSuffix( ".html" );
		resolver.setTemplateMode( TemplateMode.HTML );
		resolver.setCharacterEncoding( StandardCharsets.UTF_8.name() );
		resolver.setCacheable( false );
		return resolver;
	}

	private ITemplateResolver textTemplateResolver() {
		SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
		resolver.setApplicationContext( this.applicationContext );
		resolver.setPrefix( "classpath:/templates/" );
		resolver.setSuffix( ".txt" );
		resolver.setTemplateMode( TemplateMode.TEXT );
		resolver.setCharacterEncoding( StandardCharsets.UTF_8.name() );
		resolver.setCacheable( false );
		return resolver;
	}

	@Override
	public void setEnvironment( Environment environment ) {
		this.environment = environment;
	}

	@Override
	public void setApplicationContext( ApplicationContext applicationContext ) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
