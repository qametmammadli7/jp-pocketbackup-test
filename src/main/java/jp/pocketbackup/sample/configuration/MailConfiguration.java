package jp.pocketbackup.sample.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.nio.charset.StandardCharsets;
import java.util.Properties;

@Configuration
@PropertySource("classpath:config/application-common.properties")
public class MailConfiguration {

	@Value( "${spring.mail.protocol}" )
	private String protocol;

	@Value( "${spring.mail.host}" )
	private String host;

	@Value( "${spring.mail.port}" )
	private int port;

	@Value( "${spring.mail.username}" )
	private String username;

	@Value( "${spring.mail.password}" )
	private String password;

	@Value( "${spring.mail.properties.mail.smtp.auth}" )
	private boolean auth;

	@Value( "${spring.mail.properties.mail.smtp.starttls.enable}" )
	private boolean starttls;

	@Bean
	public JavaMailSender javaMailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

		mailSender.setProtocol( protocol );

		Properties mailProperties = new Properties();
		mailProperties.put( "mail.smtp.auth", auth );
		mailProperties.put( "mail.smtp.starttls.enable", starttls );
		mailSender.setJavaMailProperties( mailProperties );

		mailSender.setHost( host );
		mailSender.setPort( port );
		mailSender.setUsername( username );
		mailSender.setPassword( password );
		mailSender.setDefaultEncoding( StandardCharsets.UTF_8.name() );

		return mailSender;
	}
}
