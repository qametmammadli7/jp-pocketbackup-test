package jp.pocketbackup.sample.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.session.jdbc.config.annotation.web.http.EnableJdbcHttpSession;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
@EnableJdbcHttpSession( tableName = "_SESSION", maxInactiveIntervalInSeconds = ( 30 * 24 * 60 * 60 ) )
public class JdbcHttpSessionConfiguration {

	@Bean
	public PlatformTransactionManager transactionManager( DataSource dataSource ) {
		return new DataSourceTransactionManager( dataSource );
	}
}
