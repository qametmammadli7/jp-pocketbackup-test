package jp.pocketbackup.sample.datasource;


import jp.pocketbackup.sample.domain.sample.Id;

public class SampleProvider {

	public String selectAll() {
		StringBuilder builder = new StringBuilder();
		builder.append( "SELECT " );
		builder.append( "id, " );
		builder.append( "email_address, " );
		builder.append( "created_at " );
		builder.append( "FROM " );
		builder.append( "sample " );
		builder.append( "ORDER BY id " );
		return builder.toString();
	}

	public String selectById( Id id ) {
		StringBuilder builder = new StringBuilder();
		builder.append( "SELECT " );
		builder.append( "id, " );
		builder.append( "email_address, " );
		builder.append( "created_at " );
		builder.append( "FROM " );
		builder.append( "sample " );
		builder.append( "WHERE " );
		builder.append( "id = #{id.value} " );
		return builder.toString();
	}
}
