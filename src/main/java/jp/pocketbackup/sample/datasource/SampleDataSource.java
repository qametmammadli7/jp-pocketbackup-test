package jp.pocketbackup.sample.datasource;

import jp.pocketbackup.sample.domain.sample.Id;
import jp.pocketbackup.sample.domain.sample.Sample;
import jp.pocketbackup.sample.domain.sample.SampleRepository;
import jp.pocketbackup.sample.domain.sample.Samples;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository( "sampleRepository" )
@RequiredArgsConstructor
public class SampleDataSource implements SampleRepository {

	private final SampleMapper mapper;

	@Override
	public Samples findAll() {
		List<Sample> list = mapper.selectAll();
		return new Samples( list );
	}

	@Override
	public Sample findById( Id id ) {
		return mapper.selectById( id );
	}
}
