package jp.pocketbackup.sample.datasource;


import jp.pocketbackup.sample.domain.sample.Id;
import jp.pocketbackup.sample.domain.sample.Sample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

public interface SampleMapper {

	@Results( id = "base", value = {
			@Result( property = "id.value", column = "id" ),
			@Result( property = "emailAddress.value", column = "email_address" ),
			@Result( property = "createdAt.value", column = "created_at" )
	} )
	@SelectProvider( type = SampleProvider.class, method = "selectAll" )
	List< Sample > selectAll();

	@ResultMap( "base" )
	@SelectProvider( type = SampleProvider.class, method = "selectById" )
	Sample selectById(@Param( "id" ) Id id );
}
